This is the project readme. It contains some guidelines on how to get started. *The user should remove this text when
releasing to their own branch*.

# CI workflow

By using this repository, and adhering to the template, you will achieve the following points using the CI/CD workflow
included by Gitlab:

* Automatic testing
* Automatic release and versioning with tags and release notes (on master/alpha branch)
* Automatic documentation using Sphinx and Sphinx-gallery for examples (on master branch only)
* Automatic release to pypi (on master branch only)


## Automatic versioning and release
[semantic release](https://github.com/semantic-release/semantic-release) checks the commit messages and issues releases 
(major, minor, patch) if triggered. Creates release notes and adds a new `version.txt` file. Requires a `GITLAB_TOKEN`   
specified as a CI environment variable. See
[here](https://docs.gitlab.com/ee/ci/variables/#define-a-cicd-variable-in-the-ui) on how to create a variable and
[here](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) on how to create an access token.

## Automatic tests:
runs pytest on all tests in the test [folder](/tests/src). Returns error if tests fail. Saves logs to job. 

## Automatic documentation 
Builds the documentation and releases it using gitlab pages. The URL of this depends on the repo that was 
used. See [here](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html). 

## Automatic distribution 
Pushes code directly to pypi. Requires `PYPI_USERNAME` and `PYPI_PASSWORD` as 
[CI environment variables](https://docs.gitlab.com/ee/ci/variables/#define-a-cicd-variable-in-the-ui).

# How to get started

Fork this repository, then follow the steps below

## Package your files
A default package namespace exists (mynamespace), with the package (mypackage). Rename these folders to your liking.
Put your package files inside the (former) `mypackage` folder. Change the `setup.py` file to reflect the package name
and description.

## Create documentation
Put custom documentation in the [source](/docs/source) folder. Edit the [index.rst](/docs/source/index.rst) to include 
the files. Use doc-strings in the [python source](/src/mynamespace/mypackage) to automatically generate an API 
documentation. See more on the [gitlab pages](https://numerous.gitlab.io/resources/numerous-template-repository/readme) 
which contains tips on how to add more documentation, references and so on. The user should probably familiarize 
themselves with [reST](https://sphinx-tutorial.readthedocs.io/step-1/).

## Create tests
Place tests inside the [source](/tests/src) folder. An example test can be seen 
[here](/tests/src/test_my_first_test.py). Tests must be written for [pytest](https://docs.pytest.org/en) 
and should be called something along `test_{}.py`. The important thing is that the name contains the prefix `test_`.

## Configure CI environment variables

Setup the missing variables using gitlabs UI:

* `GITLAB_TOKEN`: the personal access token required for  
[semantic release](https://github.com/semantic-release/semantic-release)
* `PYPI_USERNAME`: your pypi username
* `PYPI_PASSWORD`: your pypi password

## Customize the CI scripts

If you want, you can edit the CI scripts. These include the [.gitlab-ci.yml](.gitlab-ci.yml), the 
[scripts.yml](scripts.yml). To control the flow of 
[semantic release](https://github.com/semantic-release/semantic-release) you can edit the 
[.releaserc.yml](.releaserc.yml) file.