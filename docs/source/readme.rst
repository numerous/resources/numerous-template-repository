===============================
Documentation
===============================

This is the documentation. You should probably add more, and remove the stuff below. You are encouraged to click the
`view page source <_sources/readme.rst.txt>` to see the raw file.



Sections
=================

can be created using a::

    line below like this
    ======================

subsections
------------------

can be created using a ::

    line below like this
    ---------------------

and

Subsubsections
~~~~~~~~~~~~~~~~~~~~~~

can be created using a ::

    line below like this
    ~~~~~~~~~~~~~~~~~~~~~~

.. _anchor::

Add more files to the index.rst
--------------------------------

The index.rst contains the front page of your documentation and all the links that are displayed in the table of
contents (TOC). You can add more links like this by modifying the index.rst::

    .. toctree::
       :maxdepth: 2
       :caption: Contents:

       name displayed in index <relative-link-to-file-from-source-folder.rst>

links between documents
--------------------------

You can also create links between :doc:`files <auto_examples/index>`::

    :doc:`this is a link to another file <this-is-the-filename-without-the-extension>`

more links
------------------------

You can also add links to external pages like: `click this link to learn more about reST <rest>`_.

or internal links using `anchors <anchor>`_::

    .. _anchor::
        when referencing with `anchor`_ you will be taken to this line

or even just using the `headers <Documentation>`_::

    Referencing to the `Documentation`_ header will take you to the top of this document.

.. _rest: https://sphinx-tutorial.readthedocs.io/step-1/