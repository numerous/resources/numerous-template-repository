from numerous.html_report_generator import DataFrameTable
from numerous.html_report_generator import Report
import os.path
from pathlib import Path
import pandas as pd

def test_table_in_report():
    folder = Path("./output")
    filename = 'test_table_in_report'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header='Test report header - xyz',
                           title='Test report title',
                           sub_title='Test report sub title',
                           sub_sub_title='Test report sub sub title',
                           )

    table_data = pd.DataFrame({
        'a': [1, 2, 3],
        'b': [5, 6, 7]
    })

    table = DataFrameTable(table_data, caption="test")


    report.add_blocks({'table': table})

    report.save()
    assert os.path.isfile(file)