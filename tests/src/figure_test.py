import pytest
from typing import Union

import plotly.graph_objects as go
from numerous.html_report_generator import GoFigure
from numerous.html_report_generator import Report
import os.path
from pathlib import Path

fig_data1 = dict({
        "data": [{"type": "bar",
                  "x": [1, 2, 3],
                  "y": [1, 3, 2]}],
        "layout": {"title": {"text": "A Figure Specified By Python Dictionary"}}
    })

fig_data2 = [go.Scatter({'x': [1,2,3,4], 'y': [2,-1,10,1]})]

@pytest.mark.parametrize('fig_data', [fig_data1, fig_data2])
def test_fig_in_report(fig_data: Union[dict, list[dict]]):
    folder = Path("./output")
    filename = 'test_fig_in_report'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header='Test report header - xyz',
                           title='Test report title',
                           sub_title='Test report sub title',
                           sub_sub_title='Test report sub sub title',
                           )



    fig = GoFigure(fig_data, caption="test")


    report.add_blocks({'fig': fig})

    report.save()
    assert os.path.isfile(file)

def test_update_figure_layout():
    folder = Path("./output")
    filename = 'test_fig_in_report'
    file = folder.joinpath(filename + '.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header='Test report header - xyz',
                           title='Test report title',
                           sub_title='Test report sub title',
                           sub_sub_title='Test report sub sub title',
                           )

    fig = GoFigure(figure_data=fig_data2, caption="test")

    report.add_blocks({'fig': fig})
    fig.figure_obj.update_layout({"title": {"text": "A Figure Specified By Python Dictionary"}})

    report.save()
    assert os.path.isfile(file)