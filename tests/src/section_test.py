from numerous.html_report_generator import Report, Div, Section
import pytest
import os.path
from pathlib import Path

def test_section_init():
    sec = Section("This is the section title")
    assert sec.section_title == "This is the section title"

    with pytest.raises(Exception) as e_info:
        sec = Section()
    assert "__init__() missing 1 required positional argument: 'section_title'" in str(e_info.value)


def test_section_in_report():
    folder = Path("./output")
    filename = 'test_section_in_report'
    file = folder.joinpath(filename+'.html')
    if os.path.exists(file):
        os.remove(file)

    report = Report(target_folder=folder, filename=filename)
    report.add_header_info(header = 'Test report header',
                           title = 'Test report title',
                           sub_title = 'Test report sub title',
                           sub_sub_title = 'Test report sub sub title',
                           )
    sec = Section("This is the section title")
    sec.add_content({'line1': Div('<b>Bla bla bla bla</b>')})
    report.add_blocks({'sec1': sec})
    report.save()

    assert os.path.isfile(file)


def test_html_check():
    with pytest.raises(Exception) as e_info:
        sec = Section("This is the section title")
        sec.add_content({'line1': 'Bla bla bla bla'})
        assert "Current string in content is not an html string:" in str(e_info.value)


def test_set_functions():
    report = Report()
    sec = Section("This is the section title")
    div_ = Div('<b>Bla bla bla bla</b>')
    sec.add_content({'line1': div_})
    report.add_blocks({'sec1': sec})

    assert sec.content['line1'] == div_
    assert report.blocks == {'sec1': sec}

    #Setting content of section only
    div2_ = Div('<b>La la la la</b>')
    sec.set_content({'line2': div2_})
    assert sec.content['line2'] == div2_
    assert report.blocks['sec1'].content['line2'] == div2_

    # Setting content of section and report
    div3_ = Div('<b>A a a a</b>')
    sec.set_content({'line3': div3_})
    report.set_blocks({'sec3': sec})
    assert sec.content['line3'] == div3_
    assert report.blocks['sec3'].content['line3'] == div3_