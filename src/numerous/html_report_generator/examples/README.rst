Gallery
=============

This is the gallery. Edit this file.

Requirements
-----------------
Some additional requirements are needed to run the examples. These are:

* `plotly`_ for plotting
* `pandas`_ for dataframe formatting

.. _`plotly`: https://plotly.com/
.. _`pandas`: https://pandas.pydata.org/pol