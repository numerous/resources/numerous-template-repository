# Contributing to numerous report generator

First of all welcome to you. We are so happy that you are considering to contribute to this package.

There are sevaral ways to contribute:
* Submitting your bug reports or feature requests as issues in gitlab: https://gitlab.com/numerous/report-generator/-/issues
* Implement new features or fixes and submitting them as a merge request
* Ask to be invited as a member

## Generating documentation locally:

All commands should be run from root folder.

```console
pip install sphinx-rtd-theme
```

### If the folder 'docs' doesn't exists run:


```console
sphinx-quickstart docs   
```
* Seperate source and build directories: 'n'. 
* 'make.bat' and 'Makefile' can be deleted

##### Make following changes to 'conf.py'

```console
import os
import sys
sys.path.insert(0, os.path.abspath('..'))

source_suffix = '.rst'

master_doc = 'index'

extensions = [
    'sphinx.ext.autodoc',
]

html_theme = 'sphinx_rtd_theme'

```



#### Make following changes to 'index.rst'
```console
.. include:: modules.rst
```


### Generating documentation:



```console
sphinx-apidoc -f -e -d 4 -o ./docs ./src/numerous/report
sphinx-build -b html ./docs ./docs/_build
```

Find documentation in: '/docs/_build/...'

## Testing:

#### Installation

```console
pip install pytest
pip install coverage
```
#### Running pytest

```console
pytest
```
#### Running coverage:

```console
coverage run -m pytest
coverage report
coverage html
```

Find coverage report in: '/htmlcov/index.html'